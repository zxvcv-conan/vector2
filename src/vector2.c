// ====================================================================================
//                                LICENSE INFORMATIONS
// ====================================================================================
// Copyright (c) 2020-2022 Pawel Piskorz
// Licensed under the Eclipse Public License 2.0
// See attached LICENSE file
// ====================================================================================


// =================================== INCLUDES =======================================
#include "vector2.h"
#include <math.h>


// ============================== PUBLIC DEFINITIONS ==================================

vect2D vect2D_get(point2D p1, point2D p2)
{
    vect2D vector;

    vector.x = p2.x - p1.x;
    vector.y = p2.y - p1.y;

    return vector;
}

double vect2D_length(vect2D v)
{
    return sqrt(pow(v.x, 2) + pow(v.y, 2));
}

double vect2D_scalar_product(vect2D v1, vect2D v2)
{
    return v1.x * v2.x + v1.y * v2.y;
}

double vect2D_angle(vect2D v1, vect2D v2)
{
    double tmp = (vect2D_length(v1) * vect2D_length(v2));

    if(tmp == 0)
    {
        return 0;
    }

    return acos(vect2D_scalar_product(v1, v2) / tmp);
}

void vect2D_scalar_multiply(vect2D* v, double value)
{
    v->x *= value;
    v->y *= value;
}

vect2D vect2D_sum(vect2D v1, vect2D v2)
{
    vect2D vector;

    vector.x = v1.x + v2.x;
    vector.y = v1.y + v2.y;

    return vector;
}

vect2D vect2D_normalize(vect2D v)
{
    vect2D vector;

    double v_len = vect2D_length(v);

    if(v_len == 0)
    {
        vector.x = 0;
        vector.y = 0;
    }
    else
    {
        vector.x = v.x / v_len;
        vector.y = v.y / v_len;
    }

    return vector;
}

vect2D vect2D_velocity(vect2D move, double v)
{
    double alfa;
    vect2D vel;
    vect2D sign;

    sign.x = signbit(move.x) ? -1 : 1;
    sign.y = signbit(move.y) ? -1 : 1;

    if(move.x == 0 && move.y == 0)
    {
        vel.x = 0;
        vel.y = 0;
    }
    else if(move.x == 0)
    {
        vel.x = 0;
        vel.y = v * sign.y;
    }
    else if(move.y == 0)
    {
        vel.x = v * sign.x;
        vel.y = 0;
    }
    else
    {
        alfa = atan(move.y / move.x);
        vel.x = fabs(cos(alfa) * v) * sign.x;
        vel.y = fabs(sin(alfa) * v) * sign.y;
    }

    return vel;
}
