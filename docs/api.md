


# API

[comment]: # ([[_DOCMD_USER_BLOCK_0_BEGIN]])

[comment]: # ([[_DOCMD_USER_BLOCK_0_END]])

</br>

## Structures
***

[comment]: # ([[_DOCMD_USER_BLOCK_1_BEGIN]])

[comment]: # ([[_DOCMD_USER_BLOCK_1_END]])

> ## point2D_Tag
> ***
> Definitions of 2D point object
>
> For store two dimention points as single value.
>
> ### <u><i>Members:</i></u>
> |NAME|TYPE|DESCRIPTION|
> |-|-|-|
> |x|double|X axis value|
> |y|double|Y axis value|

</br>

> ## vect2D_Tag
> ***
> Definitions of 2D vector object
>
> For store two dimention vector as single value.
>
> ### <u><i>Members:</i></u>
> |NAME|TYPE|DESCRIPTION|
> |-|-|-|
> |x|double|X axis value|
> |y|double|Y axis value|

</br>





## Typedefs
***

[comment]: # ([[_DOCMD_USER_BLOCK_4_BEGIN]])

[comment]: # ([[_DOCMD_USER_BLOCK_4_END]])

> ## point2D
> ***
> 2D point object type
>
> ### <u><i>Definition:</i></u>
> point2D_Tag
>

</br>

> ## vect2D
> ***
> 2D vector object type
>
> ### <u><i>Definition:</i></u>
> vect2D_Tag
>

</br>



</br>

## Functions
***

[comment]: # ([[_DOCMD_USER_BLOCK_5_BEGIN]])

[comment]: # ([[_DOCMD_USER_BLOCK_5_END]])

> ## vect2D_get
> ***
> Create vector P1 P2.
>
> P1 is the begining of the vecor, and P2 is the end of it.Function will return vector object based on passed values.
>
> ### <u><i>Params:</i></u>
> |NAME|TYPE|PURPOSE|DESCRIPTION|
> |-|-|-|-|
> |p1|point2D|in|X axis value of point P1.|
> |p2|point2D|in|X axis value of point P2.|
> ### <u><i>Return:</i></u>
> (vect2D) Created vector.

</br>

> ## vect2D_length
> ***
> Return length of the vector.
>
> 
>
> ### <u><i>Params:</i></u>
> |NAME|TYPE|PURPOSE|DESCRIPTION|
> |-|-|-|-|
> |v|vect2D|in|Vector which lenght we wanna get.|
> ### <u><i>Return:</i></u>
> (double) Vectors' v lenght.

</br>

> ## vect2D_scalar_product
> ***
> Return scalar product of two vectors
>
> 
>
> ### <u><i>Params:</i></u>
> |NAME|TYPE|PURPOSE|DESCRIPTION|
> |-|-|-|-|
> |v1|vect2D|in|Vector 1.|
> |v2|vect2D|in|Vector 2.|
> ### <u><i>Return:</i></u>
> (double) Vectors scalar product.

</br>

> ## vect2D_angle
> ***
> Return angle between two vectors.
>
> 
>
> ### <u><i>Params:</i></u>
> |NAME|TYPE|PURPOSE|DESCRIPTION|
> |-|-|-|-|
> |v1|vect2D|in|Vector 1.|
> |v2|vect2D|in|Vector 2.|
> ### <u><i>Return:</i></u>
> (double) Angle in radians. If any of the vectors had length=0, return 0.

</br>

> ## vect2D_scalar_multiply
> ***
> Multiply vector by scalar.
>
> Function is done in a place (on passed values).
>
> ### <u><i>Params:</i></u>
> |NAME|TYPE|PURPOSE|DESCRIPTION|
> |-|-|-|-|
> |v|vect2D*|in-out|Vector to multiply.|
> |value|double|in|Scalar value.|

</br>

> ## vect2D_sum
> ***
> Sum two vecotrs.
>
> 
>
> ### <u><i>Params:</i></u>
> |NAME|TYPE|PURPOSE|DESCRIPTION|
> |-|-|-|-|
> |v1|vect2D|in|Vector 1.|
> |v2|vect2D|in|Vector 2.|
> ### <u><i>Return:</i></u>
> (vect2D) Output of sum operation on vectors.

</br>

> ## vect2D_normalize
> ***
> Return normalized vector.
>
> Returned vector will always have length equal to 1.
>
> ### <u><i>Params:</i></u>
> |NAME|TYPE|PURPOSE|DESCRIPTION|
> |-|-|-|-|
> |v|vect2D|in|Input vector.|
> ### <u><i>Return:</i></u>
> (vect2D) Normalized vector v. If vector v has length=0, return vector (0; 0).

</br>

> ## vect2D_velocity
> ***
> Get velocity vector, based on movement and velocity.
>
> TODO[PP]: This shouldn't be here, its not sttrictly a vector operation, but a phisical2D calculations.
>
> ### <u><i>Params:</i></u>
> |NAME|TYPE|PURPOSE|DESCRIPTION|
> |-|-|-|-|
> |move|vect2D|in|Vector that represents movement.|
> |v|double|in|Velocity.|
> ### <u><i>Return:</i></u>
> (vect2D) Velocity vector that should be applied to move a point by a distance
> |CODE|DESCRIPTION|
> |-|-|
> |indicated|by the vector with velocity v.|

</br>



[comment]: # ([[_DOCMD_USER_BLOCK_6_BEGIN]])

[comment]: # ([[_DOCMD_USER_BLOCK_6_END]])

***
[Main page](../README.md) | [Repository url](https://gitlab.com/zxvcv-conan/vector2) </br>
***
<i>Generated with <b>docmd</b> Python package.</i>
