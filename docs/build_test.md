


# Building

[comment]: # ([[_DOCMD_USER_BLOCK_0_BEGIN]])
With python custom builder
==========================
```
python3 -m zxvcv.conan-toolbox build $(id -u ${USER}) $(id -g ${USER}) --upload-strategy NEVER --log debug
```

Gitlab require to login once again after a while of absence when uploading: ```conan user <username> -r gitlab -p```

Step by step
============
```
TODO[PP]: ...
```

[comment]: # ([[_DOCMD_USER_BLOCK_0_END]])


# Testing

[comment]: # ([[_DOCMD_USER_BLOCK_1_BEGIN]])
With python custom builder
==========================
```
python3 -m zxvcv.conan-toolbox build-ut --build missing
```


Step by step
============
inside root ut project directory:
```
mkdir bld && cd bld
conan install .. --build=missing -s build_type=Debug --remote=gitlab
cmake .. -DCMAKE_BUILD_TYPE=Debug
cmake --build . --target tests_ut
ctest -V
```

[comment]: # ([[_DOCMD_USER_BLOCK_1_END]])


# Generating Documentation

[comment]: # ([[_DOCMD_USER_BLOCK_2_BEGIN]])
```
python3 -m docmd generate include/vector2.h  --log debug
```

[comment]: # ([[_DOCMD_USER_BLOCK_2_END]])

[comment]: # ([[_DOCMD_USER_BLOCK_3_BEGIN]])

[comment]: # ([[_DOCMD_USER_BLOCK_3_END]])

***
[Main page](../README.md) | [Repository url](https://gitlab.com/zxvcv-conan/vector2) </br>
***
<i>Generated with <b>docmd</b> Python package.</i>
