


# Settings

- os
- compiler
- build_type
- arch
- fpu

[comment]: # ([[_DOCMD_USER_BLOCK_0_BEGIN]])

[comment]: # ([[_DOCMD_USER_BLOCK_0_END]])

</br>

# Options

|NAME|AVAILABLE VALUES|DEFAULT VALUE|
|-|-|-|
|shared|[True, False]|False|

[comment]: # ([[_DOCMD_USER_BLOCK_1_BEGIN]])

[comment]: # ([[_DOCMD_USER_BLOCK_1_END]])

</br>

# Requires


[comment]: # ([[_DOCMD_USER_BLOCK_2_BEGIN]])

[comment]: # ([[_DOCMD_USER_BLOCK_2_END]])

***
[Main page](../README.md) | [Repository url](https://gitlab.com/zxvcv-conan/vector2) </br>
***
<i>Generated with <b>docmd</b> Python package.</i>
