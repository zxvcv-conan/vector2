// ====================================================================================
//                                LICENSE INFORMATIONS
// ====================================================================================
// Copyright (c) 2020-2022 Pawel Piskorz
// Licensed under the Eclipse Public License 2.0
// See attached LICENSE file
// ====================================================================================

#ifndef ZXVCV_VECTOR2_MATH_H_
#define ZXVCV_VECTOR2_MATH_H_


// ================================== DATA TYPES ======================================

/** 2D point object type */
typedef
/** Definitions of 2D point object
 *
 * For store two dimention points as single value.
 */
struct point2D_Tag{
    /** X axis value */
    double x;

    /** Y axis value */
    double y;
}point2D;

/** 2D vector object type */
typedef
/** Definitions of 2D vector object
 *
 * For store two dimention vector as single value.
 */
struct vect2D_Tag{
    /** X axis value */
    double x;

    /** Y axis value */
    double y;
}vect2D;


// ============================== PUBLIC DECLARATIONS =================================

/** Create vector P1 P2.
 *
 * P1 is the begining of the vecor, and P2 is the end of it.
 * Function will return vector object based on passed values.
 *
 * @param p1 [in] X axis value of point P1.
 * @param p2 [in] X axis value of point P2.
 *
 * @return Created vector.
 */
vect2D vect2D_get(point2D p1, point2D p2);

/** Return length of the vector.
 *
 * @param v [in] Vector which lenght we wanna get.
 *
 * @return Vectors' v lenght.
 */
double vect2D_length(vect2D v);

/** Return scalar product of two vectors
 *
 * @param v1 [in] Vector 1.
 * @param v2 [in] Vector 2.
 *
 * @return Vectors scalar product.
 */
double vect2D_scalar_product(vect2D v1, vect2D v2);

/** Return angle between two vectors.
 *
 * @param v1 [in] Vector 1.
 * @param v2 [in] Vector 2.
 *
 * @return Angle in radians. If any of the vectors had length=0, return 0.
 */
double vect2D_angle(vect2D v1, vect2D v2);

/** Multiply vector by scalar.
 *
 * Function is done in a place (on passed values).
 *
 * @param v [in-out] Vector to multiply.
 * @param value [in] Scalar value.
 */
void vect2D_scalar_multiply(vect2D* v, double value);

/** Sum two vecotrs.
 *
 * @param v1 [in] Vector 1.
 * @param v2 [in] Vector 2.
 *
 * @return Output of sum operation on vectors.
 */
vect2D vect2D_sum(vect2D v1, vect2D v2);

/** Return normalized vector.
 *
 * Returned vector will always have length equal to 1.
 *
 * @param v [in] Input vector.
 *
 * @return Normalized vector v. If vector v has length=0, return vector (0; 0).
 */
vect2D vect2D_normalize(vect2D v);

/** Get velocity vector, based on movement and velocity.
 *
 * TODO[PP]: This shouldn't be here, its not sttrictly a vector operation, but a phisical
 *           2D calculations.
 * @param move [in] Vector that represents movement.
 * @param v [in] Velocity.
 *
 * @return Velocity vector that should be applied to move a point by a distance
 *         indicated by the vector with velocity v.
 */
vect2D vect2D_velocity(vect2D move, double v);

#endif // ZXVCV_VECTOR2_MATH_H_
