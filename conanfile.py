from conan import ConanFile
from conans.errors import ConanInvalidConfiguration
from conan.tools.cmake import CMakeToolchain, CMake, cmake_layout


class ConanPackage(ConanFile):
    name = "vector2"
    version = "0.1.5"

    # Optional metadata
    license = "Eclipse Public License - v 2.0"
    author = "Pawel Piskorz ppiskorz0@gmail.com"
    url = "https://gitlab.com/zxvcv-conan/vector2"
    description = """ Component implementing operation on 2D vectors. """
    topics = ("vector", "2D", "math", "C", "embedded")

    # Binary configuration
    settings = "os", "compiler", "build_type", "arch", "fpu"
    options = {
        "shared": [True, False]
    }
    default_options = {
        "shared": False
    }
    requires = ()

    # Sources are located in the same place as this recipe, copy them to the recipe
    exports_sources = "CMakeLists.txt", "src/*", "include/*", "unittest/*"

    def configure(self):
        # this is a C library, and does not depend on any C++ standard library
        del self.settings.compiler.libcxx
        del self.settings.compiler.cppstd

        # if not embedded
        if self.settings.arch not in ["cortex-m4", "avr"]:
            del self.settings.fpu

    def validate(self):
        # embedded
        if self.settings.os == "baremetal":
            # compiler restrictions
            if self.settings.arch == "avr" and not self.settings.compiler == "avr-gcc":
                raise ConanInvalidConfiguration("AVR microcontrollers architecture supports only avr-gcc compiler.")
            if self.settings.arch in ["cortex-m4"] and not self.settings.compiler == "arm-none-eabi-gcc":
                raise ConanInvalidConfiguration("ARM microcontrollers architecture supports only arm-none-eabi-gcc compiler.")

            if self.options.shared:
                raise ConanInvalidConfiguration("Shared library not supported on embedded systems.")

    def layout(self):
        cmake_layout(self)

    def generate(self):
        tc = CMakeToolchain(self)
        if self.settings.arch in ["cortex-m4"]:
            compilation_flags = " ".join(
                self.HW_CONF_CORTEX_M4_FLAGS + \
                self.SW_CONF_CORTEX_M4_FLAGS + \
                self.SPECS_CORTEX_M4_FLAGS
            )
            tc.variables["CMAKE_C_FLAGS"] = compilation_flags
            tc.variables["CMAKE_CXX_FLAGS"] = compilation_flags

        tc.generate()

    def build(self):
        cmake = CMake(self)
        if self.should_configure:   cmake.configure()
        if self.should_build:       cmake.build()
        # if self.should_test:        cmake.test()

    def package(self):
        self.copy("LICENSE", dst="licenses", src=".")
        cmake = CMake(self)
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = ["vector2"]
        ## preprocessor defines here
        if self.settings.arch in ["cortex-m4"]:
            common_flags = self.HW_CONF_CORTEX_M4_FLAGS + self.SW_CONF_CORTEX_M4_FLAGS
            # pure C flags
            self.cpp_info.cflags += common_flags
            # C++ compilation flags
            self.cpp_info.cxxflags += common_flags
            # linker flags
            self.cpp_info.sharedlinkflags += common_flags + self.SPECS_CORTEX_M4_FLAGS
        # self.cpp_info.exelinkflags = [] # linker flags (executables)
        # self.cpp_info.requires = None # TODO[PP]: check if there will be requires filled with proper way

        if "CC" in self.env: self.env_info.CC = self.env["CC"]
        if "CXX" in self.env: self.env_info.CXX = self.env["CXX"]

    def imports(self):
        self.copy("*.h")

    SPECS_CORTEX_M4_FLAGS = [
        # nano.specs defines the system include path and library parameters to use newlib-nano.
        "-specs=nano.specs",
        # defines that system calls should be implemented as stubs that return errors when called (-lnosys)
        "-specs=nosys.specs",
    ]

    HW_CONF_CORTEX_M4_FLAGS = [
        # select name of the target ARM processor
        "-mcpu=cortex-m4",
        # set support for Thumb instruction sets
        "-mthumb",
        # specify what floating-point hardware is available on target
        "-mfpu=fpv4-sp-d16",
        # specify floating-point ABI - allows generation of floating-point instructions and uses FPU-specific calling conventions
        "-mfloat-abi=hard",
    ]

    SW_CONF_CORTEX_M4_FLAGS = [
        "-fdata-sections",
        "-ffunction-sections",
        "-Wall",
        # "$<$<CONFIG:Debug>:-Og>"
    ]
