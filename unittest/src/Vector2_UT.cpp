
#include "gtest/gtest.h"

extern "C"
{
    #include "vector2.h"
}

class Vector2_UT: public ::testing::Test {
public:
    Vector2_UT(){}
    ~Vector2_UT(){}

    virtual void SetUp()
    {
        p1.x = 0;   p1.y = 0;
        p2.x = 2;   p2.y = 1;
        p3.x = 1.5; p3.y = 2.5;
        p4.x = -2;  p4.y = -3;
        p5.x = -1;  p5.y = 4;
    }

    virtual void TearDown() {}

    point2D p1, p2, p3, p4, p5;
};


/************************** TESTS **************************/

TEST_F(Vector2_UT, vect2D_get__from_poit_zero)
{
    vect2D vector;

    vector = vect2D_get(p1, p2);
    EXPECT_EQ(2, vector.x);
    EXPECT_EQ(1, vector.y);

    vector = vect2D_get(p1, p3);
    EXPECT_EQ(1.5, vector.x);
    EXPECT_EQ(2.5, vector.y);

    vector = vect2D_get(p1, p4);
    EXPECT_EQ(-2, vector.x);
    EXPECT_EQ(-3, vector.y);

    vector = vect2D_get(p1, p5);
    EXPECT_EQ(-1, vector.x);
    EXPECT_EQ(4, vector.y);
}

TEST_F(Vector2_UT, vect2D_get__from_common_points)
{
    vect2D vector;

    vector = vect2D_get(p2, p5);
    EXPECT_EQ(-3, vector.x);
    EXPECT_EQ(3, vector.y);

    vector = vect2D_get(p3, p2);
    EXPECT_EQ(0.5, vector.x);
    EXPECT_EQ(-1.5, vector.y);

    vector = vect2D_get(p4, p5);
    EXPECT_EQ(1, vector.x);
    EXPECT_EQ(7, vector.y);

    vector = vect2D_get(p3, p5);
    EXPECT_EQ(-2.5, vector.x);
    EXPECT_EQ(1.5, vector.y);
}

TEST_F(Vector2_UT, vect2D_length__test)
{
    vect2D vector;

    vector = vect2D_get(p1, p1);
    EXPECT_EQ(0, vect2D_length(vector));

    vector = vect2D_get(p1, p2);
    EXPECT_DOUBLE_EQ(2.2360679774997898, vect2D_length(vector));

    vector = vect2D_get(p1, p3);
    EXPECT_DOUBLE_EQ(2.9154759474226504, vect2D_length(vector));

    vector = vect2D_get(p1, p4);
    EXPECT_DOUBLE_EQ(3.6055512754639891, vect2D_length(vector));

    vector = vect2D_get(p1, p5);
    EXPECT_DOUBLE_EQ(4.1231056256176606, vect2D_length(vector));
}

TEST_F(Vector2_UT, vect2D_scalar_product__test)
{
    vect2D vector1, vector2;

    vector1 = vect2D_get(p1, p1);
    vector2 = vect2D_get(p1, p2);
    EXPECT_DOUBLE_EQ(0, vect2D_scalar_product(vector1, vector2));

    vector1 = vect2D_get(p1, p2);
    vector2 = vect2D_get(p3, p4);
    EXPECT_DOUBLE_EQ(-12.5, vect2D_scalar_product(vector1, vector2));

    vector1 = vect2D_get(p1, p3);
    vector2 = vect2D_get(p3, p5);
    EXPECT_DOUBLE_EQ(0, vect2D_scalar_product(vector1, vector2));

    vector1 = vect2D_get(p2, p5);
    vector2 = vect2D_get(p1, p4);
    EXPECT_DOUBLE_EQ(-3, vect2D_scalar_product(vector1, vector2));

    vector1 = vect2D_get(p3, p1);
    vector2 = vect2D_get(p2, p3);
    EXPECT_DOUBLE_EQ(-3, vect2D_scalar_product(vector1, vector2));

    vector1 = vect2D_get(p4, p5);
    vector2 = vect2D_get(p1, p5);
    EXPECT_DOUBLE_EQ(27, vect2D_scalar_product(vector1, vector2));
}

TEST_F(Vector2_UT, vect2D_angle__test)
{
    vect2D vector1, vector2;

    vector1 = vect2D_get(p1, p1);
    vector2 = vect2D_get(p1, p2);
    EXPECT_DOUBLE_EQ(0, vect2D_angle(vector1, vector2));

    vector1 = vect2D_get(p1, p2);
    vector2 = vect2D_get(p3, p4);
    EXPECT_DOUBLE_EQ(2.6011731533192091, vect2D_angle(vector1, vector2));

    vector1 = vect2D_get(p1, p3);
    vector2 = vect2D_get(p3, p5);
    EXPECT_DOUBLE_EQ(1.5707963267948966, vect2D_angle(vector1, vector2));

    vector1 = vect2D_get(p2, p5);
    vector2 = vect2D_get(p1, p4);
    EXPECT_DOUBLE_EQ(1.7681918866447774, vect2D_angle(vector1, vector2));

    vector1 = vect2D_get(p3, p1);
    vector2 = vect2D_get(p2, p3);
    EXPECT_DOUBLE_EQ(2.2794225989225665, vect2D_angle(vector1, vector2));

    vector1 = vect2D_get(p4, p5);
    vector2 = vect2D_get(p1, p5);
    EXPECT_DOUBLE_EQ(0.38687571773102802, vect2D_angle(vector1, vector2));
}

TEST_F(Vector2_UT, vect2D_scalar_multiply__test)
{
    vect2D vector;

    vector = vect2D_get(p1, p1);
    vect2D_scalar_multiply(&vector, 1);
    EXPECT_DOUBLE_EQ(0, vector.x);
    EXPECT_DOUBLE_EQ(0, vector.y);

    vector = vect2D_get(p1, p2);
    vect2D_scalar_multiply(&vector, 4);
    EXPECT_DOUBLE_EQ(8., vector.x);
    EXPECT_DOUBLE_EQ(4., vector.y);

    vector = vect2D_get(p1, p4);
    vect2D_scalar_multiply(&vector, 1.5);
    EXPECT_DOUBLE_EQ(-3., vector.x);
    EXPECT_DOUBLE_EQ(-4.5, vector.y);

    vector = vect2D_get(p2, p5);
    vect2D_scalar_multiply(&vector, -3);
    EXPECT_DOUBLE_EQ(9., vector.x);
    EXPECT_DOUBLE_EQ(-9., vector.y);

    vector = vect2D_get(p3, p2);
    vect2D_scalar_multiply(&vector, 0);
    EXPECT_DOUBLE_EQ(0, vector.x);
    EXPECT_DOUBLE_EQ(0, vector.y);

    vector = vect2D_get(p4, p1);
    vect2D_scalar_multiply(&vector, 7);
    EXPECT_DOUBLE_EQ(14., vector.x);
    EXPECT_DOUBLE_EQ(21., vector.y);
}

TEST_F(Vector2_UT, vect2D_sum__test)
{
    vect2D vector, vector1, vector2;

    vector1 = vect2D_get(p1, p1);
    vector2 = vect2D_get(p1, p2);
    vector = vect2D_sum(vector1, vector2);
    EXPECT_DOUBLE_EQ(2., vector.x);
    EXPECT_DOUBLE_EQ(1., vector.y);

    vector1 = vect2D_get(p1, p2);
    vector2 = vect2D_get(p3, p4);
    vector = vect2D_sum(vector1, vector2);
    EXPECT_DOUBLE_EQ(-1.5, vector.x);
    EXPECT_DOUBLE_EQ(-4.5, vector.y);

    vector1 = vect2D_get(p1, p3);
    vector2 = vect2D_get(p3, p5);
    vector = vect2D_sum(vector1, vector2);
    EXPECT_DOUBLE_EQ(-1., vector.x);
    EXPECT_DOUBLE_EQ(4., vector.y);

    vector1 = vect2D_get(p2, p5);
    vector2 = vect2D_get(p1, p4);
    vector = vect2D_sum(vector1, vector2);
    EXPECT_DOUBLE_EQ(-5., vector.x);
    EXPECT_DOUBLE_EQ(0, vector.y);

    vector1 = vect2D_get(p3, p1);
    vector2 = vect2D_get(p2, p3);
    vector = vect2D_sum(vector1, vector2);
    EXPECT_DOUBLE_EQ(-2., vector.x);
    EXPECT_DOUBLE_EQ(-1., vector.y);

    vector1 = vect2D_get(p4, p5);
    vector2 = vect2D_get(p1, p5);
    vector = vect2D_sum(vector1, vector2);
    EXPECT_DOUBLE_EQ(0, vector.x);
    EXPECT_DOUBLE_EQ(11., vector.y);
}

TEST_F(Vector2_UT, vect2D_normalize__test)
{
    vect2D vector, out_vector;

    vector = vect2D_get(p1, p1);
    out_vector = vect2D_normalize(vector);
    EXPECT_DOUBLE_EQ(0, out_vector.x);
    EXPECT_DOUBLE_EQ(0, out_vector.y);

    vector = vect2D_get(p1, p2);
    out_vector = vect2D_normalize(vector);
    EXPECT_DOUBLE_EQ(0.89442719099991586, out_vector.x);
    EXPECT_DOUBLE_EQ(0.44721359549995793, out_vector.y);

    vector = vect2D_get(p1, p3);
    out_vector = vect2D_normalize(vector);
    EXPECT_DOUBLE_EQ(0.51449575542752646, out_vector.x);
    EXPECT_DOUBLE_EQ(0.8574929257125441, out_vector.y);

    vector = vect2D_get(p1, p4);
    out_vector = vect2D_normalize(vector);
    EXPECT_DOUBLE_EQ(-0.55470019622522915, out_vector.x);
    EXPECT_DOUBLE_EQ(-0.83205029433784372, out_vector.y);

    vector = vect2D_get(p1, p5);
    out_vector = vect2D_normalize(vector);
    EXPECT_DOUBLE_EQ(-0.24253562503633297, out_vector.x);
    EXPECT_DOUBLE_EQ(0.97014250014533188, out_vector.y);

    vector = vect2D_get(p3, p4);
    out_vector = vect2D_normalize(vector);
    EXPECT_DOUBLE_EQ(-0.53687549219315922, out_vector.x);
    EXPECT_DOUBLE_EQ(-0.84366148773210747, out_vector.y);

    vector = vect2D_get(p4, p5);
    out_vector = vect2D_normalize(vector);
    EXPECT_DOUBLE_EQ(0.1414213562373095, out_vector.x);
    EXPECT_DOUBLE_EQ(0.98994949366116647, out_vector.y);
}
