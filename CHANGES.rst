Changelog
=========

0.1.5 (2022-11-09)
------------------
- Add functions: vect2D_scalar_multiply() and vect2D_sum().
- Generate missing documentation and updates.
- Unittests for functons: vect2D_scalar_multiply() and vect2D_sum().

0.1.4 (2022-11-09)
------------------
- Add function vect2D_normalize().
- Fixes for implementation function vect2D_get().
- Fix for vect2D_angle(), zero divisoin erro handling.
- Add unitttests for component.

0.1.3 (2022-11-08)
------------------
- Update conan package parameteres (remove interrupts_safe).
- Update doc strings (parameters direction add).
- Add doc string for vect2D_velocity().
- Fix in implementation vect2D_angle() and vect2D_velocity().
- Documentation generated.
- Add build profiles.
- Add unittests template.
- Add build.cfg.json file.

0.1.2 (2022-11-08)
------------------
- Update function and structure names.
- Add cdoc strings for functions, structures and typedefs.

0.1.1 (2022-11-08)
------------------
- Rename component vector -> vector2.

0.1.0 (2022-11-07)
------------------
- Initial.