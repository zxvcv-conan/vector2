


# vector2

Component implementing operation on 2D vectors.

[comment]: # ([[_DOCMD_USER_BLOCK_0_BEGIN]])

[comment]: # ([[_DOCMD_USER_BLOCK_0_END]])

***

[comment]: # ([[_DOCMD_USER_BLOCK_1_BEGIN]])

[comment]: # ([[_DOCMD_USER_BLOCK_1_END]])

</br>

## Table of contents
***
- [Package Settings](docs/settings.md) - parameters, settings and defines and requirements description
- [API reference](docs/api.md) - api description for public interface
- [Building and Testing](docs/build_test.md) - how to build and test package
- [Usage examples](docs/examples.md)
- [Known Bugs](docs/bugs.md)

</br>

## License informations
***
Eclipse Public License - v 2.0

[comment]: # ([[_DOCMD_USER_BLOCK_2_BEGIN]])

[comment]: # ([[_DOCMD_USER_BLOCK_2_END]])

</br>

## Authors
***
- Pawel Piskorz ppiskorz0@gmail.com

[comment]: # ([[_DOCMD_USER_BLOCK_3_BEGIN]])

[comment]: # ([[_DOCMD_USER_BLOCK_3_END]])

</br>

***
[Main page](./README.md) | [Repository url](https://gitlab.com/zxvcv-conan/vector2) </br>
***
<i>Generated with <b>docmd</b> Python package.</i>

